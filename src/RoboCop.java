package robocoppackage;
import robocode.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * RoboCop - a robot by (your name here)
 */
public class RoboCop extends Robot
{

	public void run() {
	
		while(true) {
			//Movimentação padrão.
			scan();
			ahead(70);
			turnGunRight(90);
			back(100);
			turnGunRight(180);
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		//Sempre mantem a distancia do outro robo
		if (e.getDistance() < 20 && getEnergy() > 50) {
			back(20);	
			fire(3);
		} 
		else {
			fire(1);
		}
		
		scan();
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		scan();
		back(10);
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) { //Desvia enquanto busca
		back(5);
		scan();
	}	
}
